package com.remotepay.redle.data

import java.io.Serializable

data class PaymentData(var price: Double, var currency: String): Serializable {
    constructor() : this(0.0, "")
}