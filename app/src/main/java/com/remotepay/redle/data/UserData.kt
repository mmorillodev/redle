package com.remotepay.redle.data

import com.google.gson.Gson

data class UserData(var email: String = "", var name: String = "", var phone: String? = null, var office: String? = null) {

    var password: String = ""
    set(str) {
        field = escapeStr(encrypt(str))
    }

    private fun encrypt(decrypted: String): String {
        val maxLiteral = 126
        val minLiteral = 33
        val byteStr = decrypted.toByteArray()

        for(i in 0 until byteStr.size) {
            byteStr[i]++

            if(byteStr[i] > maxLiteral) {
                byteStr[i] = (minLiteral + ((byteStr[i] - maxLiteral)-1)).toByte()
            }
        }

        return String(byteStr)
    }

    private fun escapeStr(str: String): String {
        return str.replace("'", "\\'")
    }

    fun toJson(): String {
        return Gson().toJson(this)
    }
}