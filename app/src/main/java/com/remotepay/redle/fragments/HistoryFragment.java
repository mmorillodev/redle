package com.remotepay.redle.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.remotepay.redle.R;
import com.remotepay.redle.adapters.HistoryAdapter;
import com.remotepay.redle.tools.HistoryData;
import com.remotepay.redle.tools.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HistoryFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    SwipeRefreshLayout refreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);

        RecyclerView recyclerHistory = view.findViewById(R.id.recyclerHistory);
        refreshLayout = view.findViewById(R.id.refreshLayout);

        List<HistoryData> dataList = new ArrayList<>();
        dataList.add(new HistoryData("Matheus", 99.99, new Date(System.currentTimeMillis())));
        dataList.add(new HistoryData("Zack", 10.00, new Date(System.currentTimeMillis())));
        dataList.add(new HistoryData("Gustavo", 3.95, new Date(System.currentTimeMillis())));
        dataList.add(new HistoryData("Ian", 5.50, new Date(System.currentTimeMillis())));
        dataList.add(new HistoryData("Pietro", 4.95, new Date(System.currentTimeMillis())));
        dataList.add(new HistoryData("Eduardo", 4.20, new Date(System.currentTimeMillis())));
        dataList.add(new HistoryData("Pedro H.", 9.95, new Date(System.currentTimeMillis())));
        dataList.add(new HistoryData("Pedro G.", 12.45, new Date(System.currentTimeMillis())));
        dataList.add(new HistoryData("Isabella", 30.95, new Date(System.currentTimeMillis())));
        dataList.add(new HistoryData("Gabriel", 20.00, new Date(System.currentTimeMillis())));
        dataList.add(new HistoryData("Maria", 19.95, new Date(System.currentTimeMillis())));

        recyclerHistory.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition =
                        (recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();

                refreshLayout.setEnabled(topRowVerticalPosition >= 0);
            }
        });
        recyclerHistory.setAdapter(new HistoryAdapter(dataList));
        recyclerHistory.setLayoutManager(new LinearLayoutManager(view.getContext()));
        /*recyclerHistory.addItemDecoration(new DividerItemDecoration(recyclerHistory.getContext(),
                DividerItemDecoration.VERTICAL));*/

        refreshLayout.setColorSchemeResources(R.color.bgColor);
        refreshLayout.setOnRefreshListener(this);

        return view;
    }

    @Override
    public void onRefresh() {
        //TODO - onRefresh
        new Thread(() -> {
            if(!Utils.isInternetAvailable(refreshLayout.getContext())) {
                ((Activity)refreshLayout.getContext())
                        .runOnUiThread(() ->
                                Utils.makeToast(refreshLayout.getContext(),
                                        getString(R.string.no_internet_msg), false)
                        );
                refreshLayout.setRefreshing(false);
            } else {
                try {
                    Thread.sleep(500);
                    refreshLayout.setRefreshing(false);
                } catch (InterruptedException e) {
                    refreshLayout.setRefreshing(false);
                }
            }
        }).start();
    }
}
