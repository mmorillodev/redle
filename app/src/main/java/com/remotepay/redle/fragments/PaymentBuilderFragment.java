package com.remotepay.redle.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.remotepay.redle.R;
import com.remotepay.redle.interfaces.OnPaymentBuilt;
import com.remotepay.redle.tools.PriceMask;

public class PaymentBuilderFragment extends Fragment {

    private OnPaymentBuilt onPaymentBuilt;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof OnPaymentBuilt)
            onPaymentBuilt = (OnPaymentBuilt) context;
        else
            throw new ClassCastException();
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_payment_builder, container, false);

        EditText editPrice = v.findViewById(R.id.editPrice);
        ImageView imgNext = v.findViewById(R.id.imgNext);

        imgNext.setOnClickListener((v_) -> onPaymentBuilt.startBarcodeActivity());
        new PriceMask().addOn(editPrice);

        return v;
    }
}