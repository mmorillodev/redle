package com.remotepay.redle.fragments;

import android.content.Context;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.remotepay.redle.R;
import com.remotepay.redle.interfaces.OnScanRequested;
import com.remotepay.redle.tools.PreferencesManager;

public class PayFragment extends Fragment {

    private final String TAG = "pay_fragment";

    private NfcAdapter nfcAdapter;
    private OnScanRequested callback;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof OnScanRequested) {
            callback = (OnScanRequested) context;
        } else {
            throw new ClassCastException("MainActivity must implement OnScanRequested");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Get NFC Adapter and check if is null, if true, inflate qrcode fragment. Inflate NFC fragment otherwise
        nfcAdapter = NfcAdapter.getDefaultAdapter(getContext());

        Log.d(TAG, "NFC adapter: " + nfcAdapter);

        if(PreferencesManager.getDefaultScanMode(getContext()).equals(getString(R.string.setting_scan_mode_nfc)) && nfcAdapter != null && nfcAdapter.isEnabled())
            return getNfcView(inflater, container);

        return getQrCodeView(inflater, container);
    }

    //TODO - NFC scanner
    private View getNfcView(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.fragment_pay_nfc, container, false);

        return view;
    }

    private View getQrCodeView(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.fragment_pay_qrcode, container, false);

        view.findViewById(R.id.scanQRCode).setOnClickListener(v -> callback.startBarcodeScanner());

        return view;
    }
}
