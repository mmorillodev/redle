package com.remotepay.redle.interfaces;

import android.view.animation.Animation;

public interface OnAnimationEnds {
    void onEnd(Animation animation);
}
