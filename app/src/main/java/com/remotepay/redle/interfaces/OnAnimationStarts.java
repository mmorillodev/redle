package com.remotepay.redle.interfaces;

import android.view.animation.Animation;

public interface OnAnimationStarts {
    void onStart(Animation animation);
}
