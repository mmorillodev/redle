package com.remotepay.redle.interfaces;

import android.view.animation.Animation;

public interface OnAnimationRepeats {
    void onRepeat(Animation animation);
}
