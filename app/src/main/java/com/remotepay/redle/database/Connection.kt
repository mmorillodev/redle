package com.remotepay.redle.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

open class Connection(context: Context,
                      private val TB_USER: String = "user",
                      private val FD_NAME: String = "name",
                      private val FD_SURNAME: String = "surname",
                      private val FD_EMAIL: String = "email",
                      private val FD_PASSWORD: String = "password",
                      private val FD_PHONE: String = "phone",
                      private val FD_OFFICE: String = "office") : SQLiteOpenHelper(context, "redle.db", null, 1) {

    override fun onCreate(db: SQLiteDatabase) {
        val sql = "CREATE TABLE IF NOT EXISTS $TB_USER (" +
                "$FD_NAME varchar(), " +
                "$FD_SURNAME varchar(), " +
                "$FD_EMAIL varchar(), " +
                "$FD_PASSWORD varchar(), " +
                "$FD_PHONE varchar(), " +
                "$FD_OFFICE varchar())"

        db.execSQL(sql)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }
}