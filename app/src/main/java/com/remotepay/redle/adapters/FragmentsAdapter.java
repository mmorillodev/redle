package com.remotepay.redle.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

public class FragmentsAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragments;

    public FragmentsAdapter(List<Fragment> list, FragmentManager manager) {
        super(manager);
        fragments = list;
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public Fragment getItem(int i) {
        return fragments.get(i);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public String toString() {
        if(fragments == null) {
            return "null";
        }

        String statement = "[";

        for(int i = 0; i < fragments.size(); i++) {
            statement += fragments.get(i).getClass() + (i == fragments.size()-1 ? "" : ", ");
        }

        return statement + "]";
    }
}
