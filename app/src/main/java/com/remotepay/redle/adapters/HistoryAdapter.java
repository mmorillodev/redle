package com.remotepay.redle.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.remotepay.redle.R;
import com.remotepay.redle.tools.HistoryData;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {

    private List<HistoryData> dataList;

    public HistoryAdapter(List<HistoryData> dataList) {
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.history_item, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        HistoryData data = dataList.get(i);

        viewHolder.txtTo.setText(data.to);
        viewHolder.txtPrice.setText(String.valueOf(data.price));
        viewHolder.txtDate.setText(new SimpleDateFormat("dd/MM/yyyy", Locale.US).format(data.date));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtTo;
        TextView txtPrice;
        TextView txtDate;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            txtTo = itemView.findViewById(R.id.txtTo);
            txtPrice = itemView.findViewById(R.id.txtPrice);
            txtDate = itemView.findViewById(R.id.txtDate);
        }
    }
}
