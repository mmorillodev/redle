package com.remotepay.redle.tools;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResponseDatas {

    public String                       response_body, errors;
    public Map<String, List<String>>    response_headers;
    public int                          http_code;

    public ResponseDatas() {
        response_body = "";
        http_code = 0;
        response_headers = new HashMap<>();
    }

    public ResponseDatas(String message, Integer http_code) {
        this();
        this.http_code = http_code;

        if(http_code >= 100 && http_code < 400)
            response_body = message;
        else
            errors = message;
    }
}
