package com.remotepay.redle.tools;

import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;

public class PriceMask implements TextWatcher, InputFilter {

    private final String TAG = "price_mask";

    public void addOn(EditText editText) {
        editText.setFilters(new InputFilter[]{this});
        editText.addTextChangedListener(this);
    }

    //Implementations of TextWatcher
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    //Implementation of InputFilter
    @Override
    public CharSequence filter(CharSequence charSequence, int i, int i1, Spanned spanned, int i2, int i3) {
        String incomingStr = charSequence.toString();
        String oldStr = spanned.toString();

        Log.d(TAG, "spanned: " + spanned.toString());
        Log.d(TAG, "Incoming string: " + incomingStr);
        Log.d(TAG, "Index of .: " + incomingStr.indexOf('.'));

        /*if(oldStr.indexOf('.') > -1)
            log(oldStr.substring(oldStr.indexOf('.')));*/

        if(oldStr.length() >= 8 || oldStr.indexOf('.') >= 0 &&
                (oldStr.substring(oldStr.indexOf('.') + 1) + incomingStr).length() > 2) {
            return "";
        }
        return null;
    }
}
