package com.remotepay.redle.tools;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

/* Class Used to handle rest services in the background
** Must be initiated using execute(String... params) method of the class AsyncTask class
*  And the class will call the subsequent methods (doInBackGround, OnPreExecute and OnPostExecute).
*  The parameters must be the URL, Method, headers(string format) and params (json format or query depending on the header).*/

public class RestClient extends AsyncTask<String, Void, String> {

    OnRequestStatusChange listeners;

    public void setOnRequestStatusChangeListeners(@NonNull OnRequestStatusChange listeners) {
        this.listeners = listeners;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            HttpRequest httpRequest = new HttpRequest(params[0], params[1])
                    .setHeaders(params[2])
                    .setParameters(params[3])
                    .fireRequest()
                    .close();
            return httpRequest.getResponse().response_body;
        } catch (IOException e) {
            return e.getMessage();
        }
    }

    @Override
    protected void onPreExecute() {
        if(listeners != null)
            listeners.onPreExecute();
    }

    @Override
    protected void onPostExecute(String response) {
        if(listeners != null)
            listeners.onPostExecute(response);
    }

    //Interface methods to handle the request status
    public interface OnRequestStatusChange {
        void onPreExecute();
        void onPostExecute(String response);
    }
}
