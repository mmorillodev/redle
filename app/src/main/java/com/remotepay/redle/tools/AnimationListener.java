package com.remotepay.redle.tools;

import android.view.animation.Animation;

import com.remotepay.redle.interfaces.OnAnimationEnds;
import com.remotepay.redle.interfaces.OnAnimationRepeats;
import com.remotepay.redle.interfaces.OnAnimationStarts;

public class AnimationListener implements Animation.AnimationListener {

    private OnAnimationStarts onStart;
    private OnAnimationRepeats onRepeat;
    private OnAnimationEnds onEnd;

    public AnimationListener setOnStart(OnAnimationStarts onStart) {
        this.onStart = onStart;
        return this;
    }

    public AnimationListener setOnRepeat(OnAnimationRepeats onRepeat) {
        this.onRepeat = onRepeat;
        return this;
    }

    public AnimationListener setOnEnd(OnAnimationEnds onEnd) {
        this.onEnd = onEnd;
        return this;
    }

    @Override
    public void onAnimationStart(Animation animation) {
        if(onStart != null) {
            onStart.onStart(animation);
        }
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if(onEnd != null) {
            onEnd.onEnd(animation);
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
        if(onRepeat != null) {
            onRepeat.onRepeat(animation);
        }
    }
}
