package com.remotepay.redle.tools;

import java.util.Date;

public class HistoryData {
    public String to;
    public double price;
    public Date date;

    public HistoryData() {
        this.to = "";
        this.price = 0.0;
        this.date = new Date();
    }

    public HistoryData(String to, double price, Date date) {
        this.to = to;
        this.price = price;
        this.date = date;
    }
}
