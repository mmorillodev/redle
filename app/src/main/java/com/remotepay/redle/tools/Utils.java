package com.remotepay.redle.tools;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

public class Utils {

    public static void hideKeyboard(Context context, View container) {
        ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE))
                .hideSoftInputFromWindow(container.getWindowToken(), 0);
    }

    public static void makeToast(Context context, String message, boolean longExposure) {
        Toast.makeText(context, message, longExposure ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT).show();
    }

    public static void makeSnackbar(View container, String message, boolean longExposure) {
        Snackbar.make(container, message, longExposure ? Snackbar.LENGTH_LONG : Snackbar.LENGTH_SHORT).show();
    }

    public static boolean isInternetAvailable(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = manager.getActiveNetworkInfo();
        return (info != null && info.isConnected());
    }

    public static boolean isPermissionGranted(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }
}
