package com.remotepay.redle.tools;

import android.content.Context;

import com.remotepay.redle.R;

public class PreferencesManager {

    public static boolean isLogged(Context context) {
        return context.getSharedPreferences(context.getString(R.string.app_info),
                Context.MODE_PRIVATE).getBoolean(context.getString(R.string.preference_is_logged_key), false);
    }

    public static void logout(Context context) {
        setPreference(context, context.getString(R.string.preference_is_logged_key), false);
    }

    public static void login(Context context) {
        setPreference(context, context.getString(R.string.preference_is_logged_key), true);
    }

    public static void isDarkModeSelected(Context context, boolean darkMode) {
        setPreference(context, context.getString(R.string.preference_dark_mode), darkMode);
    }

    public static boolean isDarkModeSelected(Context context) {
        return context.getSharedPreferences(context.getString(R.string.app_info), Context.MODE_PRIVATE)
                .getBoolean(context.getString(R.string.preference_dark_mode), false);
    }

    public static String getDefaultScanMode(Context context) {
        return context.getSharedPreferences(context.getString(R.string.app_info), Context.MODE_PRIVATE)
                .getString(context.getString(R.string.preference_scan_mode),
                        context.getString(R.string.setting_scan_mode_qrcode));
    }

    public static void setDefaultScanMode(Context context, String scanMode) {
        if(!scanMode.equals(context.getString(R.string.setting_scan_mode_qrcode)) &&
                !scanMode.equals(context.getString(R.string.setting_scan_mode_nfc)))
            throw new InvalidScanTypeException("Couldn't resolve scan mode: " + scanMode);

        setPreference(context, context.getString(R.string.preference_scan_mode), scanMode);
    }

    private static void setPreference(Context context, String key, String value) {
        context.getSharedPreferences(context.getString(R.string.app_info), Context.MODE_PRIVATE)
                .edit()
                .putString(key, value)
                .apply();
    }

    private static void setPreference(Context context, String key, boolean value) {
        context.getSharedPreferences(context.getString(R.string.app_info), Context.MODE_PRIVATE)
                .edit()
                .putBoolean(key, value)
                .apply();
    }

    private static class InvalidScanTypeException extends RuntimeException{
        InvalidScanTypeException(String msg) {
            super(msg);
        }
    }
}
