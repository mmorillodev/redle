package com.remotepay.redle.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TableRow;

import com.remotepay.redle.R;
import com.remotepay.redle.tools.PreferencesManager;

public class SettingsActivity extends AppCompatActivity
        implements AdapterView.OnItemSelectedListener,
        CompoundButton.OnCheckedChangeListener, View.OnTouchListener {

    private final String TAG = "settings_activity";

    private Spinner spinnerScanMode;
    private Switch switchViewMode;
    private TableRow rowLogout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        spinnerScanMode = findViewById(R.id.spinnerScanMode);
        switchViewMode = findViewById(R.id.switchViewMode);
        rowLogout = findViewById(R.id.rowLogout);

        spinnerScanMode.setOnItemSelectedListener(this);
        switchViewMode.setOnCheckedChangeListener(this);
        rowLogout.setOnTouchListener(this);

        setScanMode();
        setViewMode();
    }

    public void finish(View view) {
        setResult(RESULT_OK);
        finish();
    }

    private void setScanMode() {
        String[] strArr = getResources().getStringArray(R.array.arr_scan_modes);
        String selectedMode = PreferencesManager.getDefaultScanMode(this);

        for(int i = 0; i < strArr.length; i++) {
            if(strArr[i].equals(selectedMode)) {
                spinnerScanMode.setSelection(i);
                return;
            }
        }
    }

    private void setViewMode() {
        switchViewMode.setChecked(PreferencesManager.isDarkModeSelected(this));
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        Log.d(TAG, "Item: " + adapterView.getItemAtPosition(position));
        PreferencesManager.setDefaultScanMode(this, (String)adapterView.getItemAtPosition(position));
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {}

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        PreferencesManager.isDarkModeSelected(this, isChecked);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_DOWN) {

            rowLogout.setBackgroundColor(getResources().getColor(R.color.lighterGray));

        } else if(event.getAction() == MotionEvent.ACTION_UP) {
            PreferencesManager.logout(getBaseContext());

            Intent loginAct = new Intent(this, LoginActivity.class);
            startActivity(loginAct);

            finish();
        }

        return true;
    }
}
