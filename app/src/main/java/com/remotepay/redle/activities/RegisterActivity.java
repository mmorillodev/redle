package com.remotepay.redle.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.remotepay.redle.tools.PreferencesManager;
import com.remotepay.redle.tools.Utils;
import com.remotepay.redle.data.UserData;
import com.remotepay.redle.R;
import com.remotepay.redle.tools.RestClient;

import org.jetbrains.annotations.Nullable;

public class RegisterActivity extends AppCompatActivity implements RestClient.OnRequestStatusChange,
        View.OnTouchListener {

    private final String TAG = "register_activity";
    private final String URL = "https://redle.000webhostapp.com/accountCreator/";

    private EditText        editEmail, editName, editPassword;
    private Button          btnRegister;
    private LinearLayout    container;
    private ProgressDialog  dialog;
    private RestClient      restClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        editEmail       = findViewById(R.id.editEmail);
        editName        = findViewById(R.id.editName);
        editPassword    = findViewById(R.id.editPassword);
        btnRegister     = findViewById(R.id.btnRegister);
        container       = findViewById(R.id.register_container);

        editPassword.setOnTouchListener(this);
    }

    //Check the fields like email format and password length
    private boolean checkAllFields() {
        boolean statement = true;
        if(!editEmail.getText().toString().matches("^.+@[^\\.].*\\.[a-z]{2,}$")) {
            editEmail.setError(getString(R.string.invalid_email_msg));
            statement = false;
        }
        if(editName.getText().toString().length() <= 0) {
            editName.setError(getString(R.string.no_name_msg));
            statement = false;
        }
        if(editPassword.getText().toString().length() < 8) {
            editPassword.setError(getString(R.string.password_too_short_msg));
            statement = false;
        }
        return statement;
    }

    //Go to login activity
    public void login(@Nullable View view) {
        finish();
    }

    public void onClickRegisterBtn(View view) {
        //Check if all fields are valid, if true, proceed with the process
        if(!checkAllFields())
            return;

        //Test the internet connection
        if(!Utils.isInternetAvailable(this)) {
            Toast.makeText(this, R.string.no_internet_msg, Toast.LENGTH_SHORT).show();
            return;
        }

        //Passed in all the verifications, so hide the keyboard and proceed
        Utils.hideKeyboard(this, container);

        Log.d(TAG, "Started registration");

        //Create an instance of UserData to build the json containing
        // the user infos to pass as parameter to RestClient instance
        UserData handler = new UserData();
        handler.setName(editName.getText().toString());
        handler.setEmail(editEmail.getText().toString());
        handler.setPassword(editPassword.getText().toString());

        //Instance the RestClient and pass the parameters
        Log.d(TAG, "params: " + handler.toJson());
        restClient = new RestClient();
        restClient.setOnRequestStatusChangeListeners(this);
        restClient.execute(URL, "POST", "Content-Type:application/json", handler.toJson());
    }

    //Before execute the request disable interactions with the layout
    @Override
    public void onPreExecute() {
        disable();
    }

    //After execute check the response and enable layout interactions
    @Override
    public void onPostExecute(String response) {
        enable();

        Log.d(TAG, "response: " + response);
        if(response.contains("\"errors\":[\"user already exist\"]")) {
            Snackbar.make(container, R.string.user_exist_msg, Snackbar.LENGTH_LONG)
                    .setAction(R.string.do_login_msg, v -> login(null)).show();
        }

        else if(response.contains("\"response\":[\"created\"]")) {
            onRegisterSuccess();
        }
        else {
            Toast.makeText(this, getString(R.string.unknown_error_msg), Toast.LENGTH_SHORT).show();
        }
    }

    private void onRegisterSuccess() {
        PreferencesManager.login(getBaseContext());

        Intent main = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(main);

        finish();
    }

    private void enable() {
        btnRegister.setEnabled(true);
        btnRegister.setBackground(getDrawable(R.drawable.layout_btn_enabled));
        dialog.dismiss();
    }

    private void disable() {
        btnRegister.setEnabled(false);
        btnRegister.setBackground(getDrawable(R.drawable.layout_btn_disabled));
        dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setOnCancelListener(dialog -> {
            restClient.cancel(true);
            enable();
        });
        dialog.setMessage(getString(R.string.registering_msg));
        dialog.show();
    }

    //Get the click action on the password edit text and check if it was in the eye icon
    //If it was, turn the password visible
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        final int DRAWABLE_RIGHT = 2;

        if(event.getAction() == MotionEvent.ACTION_UP) {
            if(event.getRawX() >= (editPassword.getRight() - editPassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                Log.d(TAG, "touched icon: " + editPassword.getInputType());

                if(editPassword.getInputType() != InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD)
                    editPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                else
                    editPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                return true;
            }
        }
        v.performClick();
        return false;
    }
}