package com.remotepay.redle.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.remotepay.redle.tools.PreferencesManager;
import com.remotepay.redle.tools.Utils;
import com.remotepay.redle.data.UserData;
import com.remotepay.redle.R;
import com.remotepay.redle.tools.RestClient;

import org.jetbrains.annotations.Nullable;

public class LoginActivity extends AppCompatActivity implements RestClient.OnRequestStatusChange,
                                                        View.OnTouchListener {

    private final String TAG = "login_activity";
    private final String URL = "https://redle.000webhostapp.com/accountFinder/";

    private EditText            editEmail;
    private EditText            editPassword;
    private Button              btnLogin;
    private ProgressDialog      progressDialog;
    private ConstraintLayout    container;
    private RestClient          restClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editEmail       = findViewById(R.id.editEmail);
        editPassword    = findViewById(R.id.editPassword);
        btnLogin        = findViewById(R.id.btnLogin);
        container       = findViewById(R.id.login_container);

        editPassword.setOnTouchListener(this);
    }

    public void onClickLoginBtn(View v) {
        if(!Utils.isInternetAvailable(this)) {
            Snackbar.make(container, R.string.no_internet_msg, Snackbar.LENGTH_SHORT).show();
            return;
        }

        if(!checkAllFields())
            return;

        //After all verifications hide the keyboard and start login processes
        Utils.hideKeyboard(this, container);

        Log.d(TAG, "Started login");

        //Build the json containing the user infos
        UserData handler = new UserData();

        handler.setEmail(editEmail.getText().toString());
        handler.setPassword(editPassword.getText().toString());

        //Pass as parameter to the RestClient Object
        restClient = new RestClient();
        restClient.setOnRequestStatusChangeListeners(this);
        restClient.execute(URL, "POST", "Content-Type:application/json", handler.toJson());
    }

    //Go to register activity
    public void gotoRegister(@Nullable View view) {
        Intent registerAct = new Intent(this, RegisterActivity.class);
        startActivity(registerAct);
    }

    //Before execute the request disable interactions with the layout
    @Override
    public void onPreExecute() {
        disable();
    }

    //After execute check the response and enable layout interactions
    @Override
    public void onPostExecute(String resp) {
        enable();
        Log.d(TAG, "login response: " + resp);
        if(resp.contains("\"authenticated\":true"))
            onLoginSuccess();
        else
            onLoginFailed(resp);
    }

    private void disable() {
        btnLogin.setEnabled(false);
        btnLogin.setBackground(getDrawable(R.drawable.layout_btn_disabled));
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(true);
        progressDialog.setOnCancelListener(dialog -> {
            restClient.cancel(true);
            enable();
        });
        progressDialog.setMessage(getString(R.string.logging_msg));
        progressDialog.show();
    }

    private void enable() {
        btnLogin.setEnabled(true);
        btnLogin.setBackground(getDrawable(R.drawable.layout_btn_enabled));
        progressDialog.dismiss();
    }

    //On Login Success set shared preferences as logged and move to main activity
    private void onLoginSuccess() {
        progressDialog.dismiss();

        PreferencesManager.login(getBaseContext());

        Intent main = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(main);

        finish();
    }

    //On Login failed show the user the reason
    private void onLoginFailed(String response) {

        Log.d(TAG, response);
        if(response.contains("\"user_exist\":false}")) {
            Log.d(TAG, "Email available");
            Snackbar.make(container, R.string.user_not_exists_msg, Snackbar.LENGTH_LONG)
                    .setAction(R.string.create_acc_msg, v -> gotoRegister(null)).show();
        }
        else {
            Toast.makeText(this, R.string.invalid_grant_msg, Toast.LENGTH_SHORT)
                    .show();
        }
    }

    //Get the click action on the password edit text and check if it was in the eye icon
    //If it was, turn the password visible
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        final int DRAWABLE_RIGHT = 2;

        if(event.getAction() == MotionEvent.ACTION_UP) {
            if(event.getRawX() >= (editPassword.getRight() - editPassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                if(editPassword.getInputType() != InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD)
                    editPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                else
                    editPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                return true;
            }
        }
        v.performClick();
        return false;
    }

    private boolean checkAllFields() {
        boolean statement = true;
        if(editEmail.getText().length() <= 0) {
            editEmail.setError(getString(R.string.invalid_email_msg));
            statement = false;
        }
        if(editPassword.getText().toString().length() < 8) {
            editPassword.setError(getString(R.string.password_too_short_msg));
            statement = false;
        }
        return statement;
    }
}