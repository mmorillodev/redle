package com.remotepay.redle.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.remotepay.redle.R;
import com.remotepay.redle.tools.PreferencesManager;

public class SplashScreenActivity extends AppCompatActivity implements Runnable {

    private final String TAG = "activity_splash_screen";

    private TextView txtAppNameSt;
    private TextView txtAppName;
    private ImageView imgLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        Log.d(TAG, "started splash screen");

        txtAppNameSt = findViewById(R.id.txtAppNameSt);
        txtAppName = findViewById(R.id.txtAppName);
        imgLoading = findViewById(R.id.imgLoading);

        new Thread(this).start();
    }

    @Override
    public void run() {
        Animation loadingAnimation = AnimationUtils.loadAnimation(this, R.anim.rotate_infinity);
        imgLoading.startAnimation(loadingAnimation);

        //on start app processes
        try {
            //simulate a task
            Thread.sleep(2000);
            loadingAnimation.cancel();
        } catch (InterruptedException e) {}

        //Everything done
        runOnUiThread(this::startAnimation);
    }

    private void startAnimation() {
        imgLoading.setVisibility(View.GONE);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float animationMovement = metrics.widthPixels/2 -
                ((LinearLayout.LayoutParams)imgLoading.getLayoutParams()).leftMargin;

        ObjectAnimator animatorSt = ObjectAnimator.ofFloat(txtAppNameSt,
                "translationX", animationMovement).setDuration(700);

        animatorSt.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                float newAnimationMovement = animationMovement -
                        ((LinearLayout.LayoutParams)txtAppName.getLayoutParams()).leftMargin;

                ObjectAnimator animator = ObjectAnimator.ofFloat(txtAppName,
                        "translationX", newAnimationMovement).setDuration(700);

                animator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if(PreferencesManager.isLogged(getApplicationContext())) {
                            Intent main = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(main);
                            finish();
                        } else {
                            Intent login = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(login);
                            finish();
                        }
                    }
                });
                animator.start();
            }
        });
        animatorSt.start();
    }
}
