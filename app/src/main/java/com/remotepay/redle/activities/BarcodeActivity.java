package com.remotepay.redle.activities;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.remotepay.redle.R;
import com.remotepay.redle.data.PaymentData;

public class BarcodeActivity extends AppCompatActivity {

    private final String TAG = "activity_barcode";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode);

        PaymentData info = (PaymentData) getIntent().getSerializableExtra("payment_data");

        ImageView imgBarCode = findViewById(R.id.imgBarCode);
        TextView txtScanMsg = findViewById(R.id.txtScanMsg);

        imgBarCode.setImageBitmap(getQRCode(info));
        txtScanMsg.append(String.valueOf(info.getPrice()));
    }

    public void finish(View view) {
        setResult(RESULT_OK);
        finish();
    }

    private Bitmap getQRCode(PaymentData content) {
        final int BAR_SIZE = 1200;

        QRCodeWriter writer = new QRCodeWriter();

        try {
            BitMatrix bitMatrix = writer.encode(new Gson().toJson(content),
                    BarcodeFormat.QR_CODE, BAR_SIZE, BAR_SIZE);

            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();

            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);

            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }
            return bmp;
        } catch (WriterException e) {
            Log.e(TAG, e.getMessage());
            return Bitmap.createBitmap(BAR_SIZE, BAR_SIZE, Bitmap.Config.RGB_565);
        }
    }
}
