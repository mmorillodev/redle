package com.remotepay.redle.activities;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;

import com.remotepay.redle.R;
import com.remotepay.redle.data.PaymentData;
import com.remotepay.redle.fragments.PaymentBuilderFragment;
import com.remotepay.redle.adapters.FragmentsAdapter;
import com.remotepay.redle.fragments.HistoryFragment;
import com.remotepay.redle.fragments.PayFragment;
import com.remotepay.redle.interfaces.OnPaymentBuilt;
import com.remotepay.redle.interfaces.OnScanRequested;
import com.remotepay.redle.tools.AnimationListener;
import com.remotepay.redle.tools.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MainActivity extends AppCompatActivity implements
        BottomNavigationView.OnNavigationItemSelectedListener,
        ViewPager.OnPageChangeListener,
        OnPaymentBuilt,
        OnScanRequested {

    private final String TAG = "main_activity";

    private final int BARCODE_CREATOR_REQUEST_CODE  = 0;
    private final int BARCODE_READER_REQUEST_CODE   = 1;
    private final int SETTINGS_REQUEST_CODE         = 2;
    private final int REQUEST_PERMISSIONS_CODE      = 1;

    private final int OPT_PAY     = 0;
    private final int OPT_RECEIVE = 1;
    private final int OPT_HISTORY = 2;

    private ViewPager               pager;
    private BottomNavigationView    navigationView;
    private ImageView               imgSettings;

    //On Main activity created, Create a list with all the fragments and add to the fragment adapter.
    //Add the adapter to the view pager and set current item as index 0
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pager = findViewById(R.id.fragment_container);
        navigationView = findViewById(R.id.navigation_view);
        imgSettings = findViewById(R.id.imgSettings);

        List<Fragment> fragments = new ArrayList<Fragment>(){{
            add(new PayFragment());
            add(new PaymentBuilderFragment());
            add(new HistoryFragment());
        }};

        pager.setAdapter(new FragmentsAdapter(fragments, getSupportFragmentManager()));
        pager.setCurrentItem(OPT_RECEIVE);

        navigationView.setOnNavigationItemSelectedListener(this);
        pager.addOnPageChangeListener(this);
    }

    public void gotoSettings(View view) {
        Log.d(TAG, "Settings icon clicked!");
        Animation rotate = AnimationUtils.loadAnimation(this, R.anim.rotate);
        rotate.setAnimationListener(new AnimationListener().setOnStart(animation -> {

            Intent settingsActivity = new Intent(view.getContext(), SettingsActivity.class);
            startActivityForResult(settingsActivity, SETTINGS_REQUEST_CODE,
                    ActivityOptions.makeSceneTransitionAnimation((Activity) view.getContext()).toBundle());

        }));
        imgSettings.startAnimation(rotate);
    }

    //Event called when BottomNavigationView item is clicked
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        pager.setCurrentItem(translateItem(menuItem.getItemId()));
        return true;
    }

    private int translateItem(int idOrIndex) {
        switch (idOrIndex){
            case OPT_PAY:
                return R.id.nav_pay;
            case OPT_RECEIVE:
                return R.id.nav_charge;
            case OPT_HISTORY:
                return R.id.nav_history;
            case R.id.nav_pay:
                return OPT_PAY;
            case R.id.nav_charge:
                return OPT_RECEIVE;
            case R.id.nav_history:
                return OPT_HISTORY;
        }
        return -1;
    }
    //Implementations of interface ViewPager.OnPageChangeListener

    @Override
    public void onPageScrolled(int i, float v, int i1) {}
    //Event called when a new page of the view pager is selected

    @Override
    public void onPageSelected(int i) {
        navigationView.setSelectedItemId(translateItem(i));
    }

    @Override
    public void onPageScrollStateChanged(int i) {}

    @Override
    public void startBarcodeActivity() {
        //Hide keyboard
        Utils.hideKeyboard(this, pager);

        EditText editPrice = findViewById(R.id.editPrice);
        String editPriceValue = editPrice.getText().toString();

        if(editPriceValue.length() > 0 && Double.parseDouble(editPriceValue) > 1) {
            Intent receiveActivity = new Intent(this, BarcodeActivity.class);

            PaymentData data = new PaymentData();
            data.setPrice(Double.parseDouble(editPriceValue));
            data.setCurrency("BRL");

            receiveActivity.putExtra("payment_data", data);

            startActivityForResult(receiveActivity, BARCODE_CREATOR_REQUEST_CODE,
                    ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        } else {
            Snackbar.make(pager, getString(R.string.msg_invalid_price), Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void startBarcodeScanner() {
        Log.d(TAG, "Camera access granted? " +
                Utils.isPermissionGranted(this, Manifest.permission.CAMERA));

        if(!Utils.isPermissionGranted(this, Manifest.permission.CAMERA)) {
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                Utils.makeSnackbar(pager, this.getString(R.string.msg_request_permission), true);
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA}, REQUEST_PERMISSIONS_CODE);
            }
        } else {
            moveToBarcodeScanner();
        }

    }

    private void moveToBarcodeScanner() {
        try {
            Intent intent = new Intent("com.google.zxing.client.android.SCAN");
            intent.putExtra("SCAN_MODE", "QR_CODE_MODE"); // "PRODUCT_MODE for bar codes

            startActivityForResult(intent, BARCODE_READER_REQUEST_CODE,
                    ActivityOptions.makeSceneTransitionAnimation(this).toBundle());

        } catch (Exception e) {
            Uri marketUri = Uri.parse("market://details?id=com.google.zxing.client.android");
            Intent marketIntent = new Intent(Intent.ACTION_VIEW,marketUri);
            startActivity(marketIntent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == REQUEST_PERMISSIONS_CODE) {
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "Permission granted!");
                moveToBarcodeScanner();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == BARCODE_CREATOR_REQUEST_CODE) {
            if(resultCode == RESULT_OK) {
                //clear price edit text
                ((EditText)findViewById(R.id.editPrice)).setText("");
            }
        } else if(requestCode == BARCODE_READER_REQUEST_CODE) {
            if(resultCode == Activity.RESULT_OK) {
                Log.d(TAG, "Activity result: " + (data != null ? data.toString() : "null"));

                String contents = Objects.requireNonNull(data).getStringExtra("SCAN_RESULT");
                Utils.makeToast(this, contents, false);

            } else if(resultCode == Activity.RESULT_CANCELED) {
                Utils.makeToast(this, "Operation canceled", false);
            }
        } else if(requestCode == SETTINGS_REQUEST_CODE) {
            //TODO - handler settings result
            Log.d(TAG, "Returned from settings activity");
        }
    }
}